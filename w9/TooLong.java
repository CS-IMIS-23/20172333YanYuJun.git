package w9;
//-------------------------------------------------------------------------------
// TooLong.java   Author:YanYuJun
//
//编写一个字符过长的错误。
//-------------------------------------------------------------------------------
public class TooLong extends Exception
{
    TooLong(String message)
    {
        super(message);
    }
}
