package w9;

import java.util.Scanner;

//------------------------------------------------------------------
// pp11.2 Author:YanYuJun
//
//更改pp11.1的处理方式，捕获异常并输入一条信息后继续处理后面的内容。
//------------------------------------------------------------------
public class pp112 {
    public static void main(String[] args) throws TooLong {
        final int Max = 20;
        String Q = null ;
        Scanner scan = new Scanner(System.in);
        Scanner scan2 = new Scanner(System.in);
        while (Q=="do") ;
        {
            System.out.println("请输入字符串不大于20的语句");
            TooLong problem = new TooLong("该字符串大于20了。");
            String A = scan.nextLine();

            int B = A.length();
            try {
                if (B > 20)
                    throw problem;

            } catch (TooLong exception) {
                System.out.println("你输入的字符超过20了");
            }
            finally {
                System.out.println("是否继续？如果结束请输入Done,继续输入do");
                Q = scan2.nextLine();
            }
        }
        System.out.println("结束");
    }
}

