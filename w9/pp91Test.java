package w9;

import java.util.Scanner;

//-----------------------------------------------
//pp11.1 Author:YanYuJun
//
//测试TooLong.java的异常抛出，当字符超过20时输出异常
//
//------------------------------------------------
public class pp91Test
{
    public static void main(String[]args) throws TooLong {
        final int Max =20;
        String Q = null;
        Scanner scan =new Scanner(System.in);
        while (Q=="do");
        {
            System.out.println("请输入字符串不大于20的语句");
            TooLong problem = new TooLong("该字符串大于20了。");
            String A = scan.nextLine();
            int B = A.length();
            if (B > 20)
                throw problem;
            System.out.println("是否继续？如果结束请输入Done,继续输入do");
            Q=scan.nextLine();
        }
        System.out.println("结束");

    }
}
