package w9;
//------------------------------------------------------
//Author:YanYuJun
//第五章PalindromeTester的递归版本
//------------------------------------------------------

import java.util.Scanner;
    public class PalindromeTesterX
    {
        //-----------------------------------
        //  Tests strings to see if they are palindromes.
        //------------------------------------------------------
        static String str;
        static int left;
        static int right;
        public static void main(String[]args)
        {
         PalindromeTesterX A =new PalindromeTesterX();
         A.HuiwenT();
         A.E();
        }

        public  void HuiwenT()
        {
            while(str.charAt(left) == str.charAt(right)&& left <right)
            {
            left++;
            right++;
            HuiwenT();
            }
        }
        public  PalindromeTesterX()
        {
            Scanner scan = new Scanner(System.in);
            System.out.println("Enter a potential palindrome:");
            str = scan.nextLine();
             left = 0;
            right = str.length() -1;
        }
        public void E()
        {
            if (left<right)

                System.out.println("That string is Not a palindrome.");

            else
                System.out.println("That string IS a palindrome.");
        }
    }


