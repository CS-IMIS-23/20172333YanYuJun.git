package w9;

import java.util.Scanner;

//------------------------------------------------------
//pp12.9 Author:YanYuJun
//
//实现PP12.9的要求，输出Pascal三角形的第N行。
//------------------------------------------------------
public class sanjiao
{
    public static void main(String[]args)
    {
        int N;
        Scanner scan =new Scanner(System.in);
        System.out.println("你想要哪一行的数值？");
        N =scan.nextInt();
        Pascal(N);
    }
    public static void Pascal(int num)
    {
        int[][] value = new int[num][num];
        for (int i =0;i<num;i++)
        {
            for (int j =0;j<=i;j++)
            {
                System.out.print(get(i,j));
            }
            System.out.println();
        }
    }

    private static int get(int i, int j )
    {
        if (j ==0||i==j)
            return 1;
        return get(i-1,j-1) +get(i-1,j);
    }
}
