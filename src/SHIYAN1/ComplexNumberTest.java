package src.SHIYAN1;



import junit.framework.TestCase;
import org.junit.Test;
import src.shiyan.ComplexNumber;

public class ComplexNumberTest extends TestCase {
    ComplexNumber c1 = new ComplexNumber(3,5);
    ComplexNumber c2 = new ComplexNumber(3,5);
    double a = 5;
    @Test
    public void testAdd1() throws Exception {
        c1.ComplexAdd(c2);
        assertEquals(6.0, c1.getRealPart());
        assertEquals(10.0, c1.getImaginPart());
    }
    @Test
    public void testAdd2() throws Exception {
        c1.ComplexAdd(a);
        assertEquals(8.0, c1.getRealPart());
        assertEquals(5.0, c1.getImaginPart());
    }
    @Test
    public void testMinus1() throws Exception {
        c1.ComplexMinus(c2);
        assertEquals(0.0, c1.getRealPart());
        assertEquals(0.0, c1.getImaginPart());
    }
    public void testMinus2() throws Exception {
        c1.ComplexMinus(a);
        assertEquals(-2.0, c1.getRealPart());
        assertEquals(5.0, c1.getImaginPart());
    }
    @Test
    public void testMulti1() throws Exception {
        c1.ComplexMulti(c2);
        assertEquals(9.0, c1.getRealPart());
        assertEquals(25.0, c1.getImaginPart());
    }
    public void testMulti2() throws Exception {
        c1.ComplexMulti(a);
        assertEquals(15.0, c1.getRealPart());
        assertEquals(5.0, c1.getImaginPart());
    }
}