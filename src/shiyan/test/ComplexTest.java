package src.shiyan.test;

import junit.framework.TestCase;
import org.junit.Test;
import src.shiyan.Complex;

public class ComplexTest extends TestCase {
    Complex a = new Complex(1, 2);
    Complex b = new Complex(3, 4);

    @Test
    public void testEquals() {
        assertEquals(true, a.equals(1, 2));
        assertEquals(false, a.equals(1, 4));
    }

    @Test
    public void testComplexAdd() {
        assertEquals(new Complex(3, 6).toString(), a.ComplexAdd(new Complex(2, 4)).toString());
        assertEquals(new Complex(1, 3).toString(), a.ComplexAdd(new Complex(0, 1)).toString());
        assertEquals(new Complex(2, 2).toString(), a.ComplexAdd(new Complex(1, 0)).toString());
        assertEquals(new Complex(1, 2).toString(), a.ComplexAdd(new Complex(0, 0)).toString());
        assertEquals(new Complex(-1, 3).toString(), a.ComplexAdd(new Complex(-2, 1)).toString());
        assertEquals(new Complex(2, 0).toString(), a.ComplexAdd(new Complex(1, -2)).toString());
        assertEquals(new Complex(-2, -1).toString(), a.ComplexAdd(new Complex(-3, -3)).toString());
    }

    @Test
    public void testComplexSub() {
        assertEquals(new Complex(1, 0).toString(), b.ComplexSub(new Complex(2, 4)).toString());
        assertEquals(new Complex(3, 3).toString(), b.ComplexSub(new Complex(0, 1)).toString());
        assertEquals(new Complex(2, 4).toString(), b.ComplexSub(new Complex(1, 0)).toString());
        assertEquals(new Complex(3, 4).toString(), b.ComplexSub(new Complex(0, 0)).toString());
        assertEquals(new Complex(5, 3).toString(), b.ComplexSub(new Complex(-2, 1)).toString());
        assertEquals(new Complex(2, 6).toString(), b.ComplexSub(new Complex(1, -2)).toString());
        assertEquals(new Complex(6, 7).toString(), b.ComplexSub(new Complex(-3, -3)).toString());
    }

    @Test
    public void testComplexMulti() {
        assertEquals(new Complex(-6, 8).toString(), a.ComplexMulti(new Complex(2, 4)).toString());
        assertEquals(new Complex(-2, 1).toString(), a.ComplexMulti(new Complex(0, 1)).toString());
        assertEquals(new Complex(1, 2).toString(), a.ComplexMulti(new Complex(1, 0)).toString());
        assertEquals(new Complex(0, 0).toString(), a.ComplexMulti(new Complex(0, 0)).toString());
        assertEquals(new Complex(-4, -3).toString(), a.ComplexMulti(new Complex(-2, 1)).toString());
        assertEquals(new Complex(5, 0).toString(), a.ComplexMulti(new Complex(1, -2)).toString());
        assertEquals(new Complex(3, -9).toString(), a.ComplexMulti(new Complex(-3, -3)).toString());
    }

    @Test
    public void testComplexDiv() {
        assertEquals(new Complex(1.1, -0.2).toString(), b.ComplexDiv(new Complex(2, 4)).toString());
        assertEquals(new Complex(4, -3).toString(), b.ComplexDiv(new Complex(0, 1)).toString());
        assertEquals(new Complex(3, 4).toString(), b.ComplexDiv(new Complex(1, 0)).toString());
        assertEquals(new Complex(-0.4, -2.2).toString(), b.ComplexDiv(new Complex(-2, 1)).toString());
        assertEquals(new Complex(-1, 2).toString(), b.ComplexDiv(new Complex(1, -2)).toString());
        assertEquals(new Complex(-1.3, -0.9).toString(), b.ComplexDiv(new Complex(-3, -1)).toString());
    }


}
