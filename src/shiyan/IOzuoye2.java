package src.shiyan;

import java.io.*;
import java.util.Scanner;
import java.util.StringTokenizer;

public class IOzuoye2
{
    public static void main(String[] args) throws IOException
    {
        String Str1,Str4,Str3,Str5;
        int q = 0,w = 0,e = 0;
        try
        {
            Scanner scan = new Scanner(System.in);
            File file = new File("sort.txt");
            OutputStream outputstream = new FileOutputStream(file);
            BufferedOutputStream bufferedoutputstream = new BufferedOutputStream(outputstream);

            if (!file.exists())
            {
                file.createNewFile();
            }

            System.out.print("Enter some number: ");
            Str1 = scan.nextLine();
            StringTokenizer strs = new StringTokenizer(Str1," ");
            int length = strs.countTokens();
            int[] Str2 = new int[length];

            while (strs.hasMoreTokens())
            {
                for(int y = 0; y < length; y++)
                {
                    String string = strs.nextToken();
                    Str2[y] = Integer.parseInt(string);
                }
            }

            Str3 = "";
            for(int str :Str2)
            {
                Str3 += str + "  ";
            }
            System.out.println("排序前：" + Str3);
            bufferedoutputstream.write(Str3.getBytes());
            bufferedoutputstream.flush();

            Reader reader = new FileReader(file);
            BufferedReader bufferedreader = new BufferedReader(reader);
            StringTokenizer strss = new StringTokenizer(bufferedreader.readLine());

            while(strss.hasMoreTokens())
            {
                Str2[e] = Integer.parseInt(strss.nextToken());
                e++;
            }

            sorting(Str2);

            Str5 = "";
            for(int str : Str2)
            {
                Str5 += str + "  ";
            }
            System.out.println("排序后：" + Str5);
            Str4 = "\n" + Str5;
            bufferedoutputstream.write(Str4.getBytes());
            bufferedoutputstream.flush();
            bufferedoutputstream.close();
        }
        catch (IOException r)
        {
            System.out.println("错误!!!\n" + r.getMessage());
        }
        catch (NumberFormatException r)
        {
            System.out.println("错误!!!\n" + r.getMessage());
        }
    }

    private static void sorting(int[] str)
    {
        for(int i = 0; i <= str.length - 1; i++)
        {
            int temp = str[i];
            for(int j = i + 1; j < str.length; j++)
                if (str[i] > str[j])
                {
                    str[i] = str[j];
                    str[j] = temp;
                }
        }
    }
}