package src.shiyan;
public class ComplexNumber {
    private double realPart;
    private double imaginPart;
    public ComplexNumber(){
        this.realPart = 0.0;
        this.imaginPart = 0.0;
    }
    public ComplexNumber(double r, double i){
        this.realPart = r;
        this.imaginPart = i;
    }
    public double getRealPart(){
        return realPart;
    }
    public double getImaginPart(){
        return imaginPart;
    }
    public void setRealPart(double d){
        this.realPart = d;
    }
    public void setImaginPart(double d){
        this.imaginPart = d;
    }
    public void ComplexAdd(ComplexNumber c){
        this.realPart += c.realPart;
        this.imaginPart += c.imaginPart;
    }
    public void ComplexAdd(double c){
        this.realPart += c;
    }
    public void ComplexMinus(ComplexNumber c){
        this.realPart -= c.realPart;
        this.imaginPart -= c.imaginPart;
    }
    public void ComplexMinus(double c){
        this.realPart -= c;
    }
    public void ComplexMulti(ComplexNumber c){
        this.realPart *= c.realPart;
        this.imaginPart *= c.imaginPart;
    }
    public void ComplexMulti(double c){
        this.realPart *= c;
    }
    @Override
    public String toString(){
        return String.format("%f + %fi", this.getRealPart(),this.getImaginPart());
    }
    
}