package src.shiyan.shiyan5.shiyan5;

import java.util.Stack;

public class MyStack<T> extends Stack<T> {
    private Object element[];
    private int top;
    public MyStack(int size){
        this.element = new Object[Math.abs(size)];
        this.top = -1;
    }
    public MyStack() {
        this(64);
    }
    @Override
    public boolean isEmpty() {
        return this.top == -1;
    }
    @Override
    public T push(T x) {
        if(x==null)
            return x;
        if(this.top == element.length-1){
            Object[] temp = this.element;
            this.element = new Object[temp.length*2];
            for(int i = 0; i < temp.length; i++)
                this.element[i] = temp[i];
        }
        this.top++;
        this.element[this.top] = x;
        return x;
    }
    @Override
    public T pop() {
        return this.top==-1 ? null:(T)this.element[this.top--];
    }
    public T get() {
        return this.top==-1 ? null:(T)this.element[this.top];
    }
}

