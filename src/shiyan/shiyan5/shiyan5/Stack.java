package src.shiyan.shiyan5.shiyan5;

public interface Stack<T> {
    boolean isEmpty();
    void push(T x);
    T pop();
    T get();
}

