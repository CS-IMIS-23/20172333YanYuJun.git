package src.shiyan.shiyan5;

import java.util.StringTokenizer;
import java.util.Stack;

public class MyDC {
    /**
     * 常数加符号
     */
    private final char ADD = '+';
    /**
     * 常数加法
     */
    private final char SUBTRACT = '-';
    /**
     * 常数减法
     */
    private final char MULTIPLY = '*';
    /**
     * 常数乘法
     */
    private final char DIVIDE = '/';
    /**
     * 除法
     */
    private Stack<Integer> stack;

    public MyDC() {
        stack = new Stack<Integer>();
    }

    public int evaluate(String expr) {
        int op1, op2, result = 0;
        String token;
        StringTokenizer tokenizer = new StringTokenizer(expr);
        while (tokenizer.hasMoreTokens()) {
            token = tokenizer.nextToken();

            //如果是运算符，调用isOperator
            if (isOperator(token)) {
                //从栈中弹出操作数2
                op2 = stack.pop();
                //从栈中弹出操作数1
                op1 = stack.pop();
                //根据运算符和两个操作数调用evalSingleOp计算result;
                result=evalSingleOp(token.charAt(0), op1, op2);
                //计算result入栈;
                stack.push(result);
            } else//如果是操作数
                //操作数入栈;
                stack.push(Integer.parseInt(token));
        }

        return result;
    }

    private boolean isOperator(String token) {
        return (token.equals("+") || token.equals("-") ||
                token.equals("*") || token.equals("/"));
    }

    private int evalSingleOp(char operation, int op1, int op2) {
        int result = 0;

        switch (operation) {
            case ADD:
                result = op1 + op2;
                break;
            case SUBTRACT:
                result = op1 - op2;
                break;
            case MULTIPLY:
                result = op1 * op2;
                break;
            case DIVIDE:
                result = op1 / op2;
        }

        return result;
    }
}