package src.shiyan;

public class Complex
{
    private double RealPart, ImagePart;


    public Complex()
    {
        RealPart = 0.0;
        ImagePart = 0.0;
    }

    public Complex(double R, double I)
    {
        RealPart = R;
        ImagePart = I;
    }

    public boolean equals(double H, double J)
    {
        return (RealPart == H && ImagePart == J);
    }

    public double getRealPart()
    {
        return RealPart;
    }

    public double getImagePart()
    {
        return ImagePart;
    }

    public Complex ComplexAdd(Complex a)
    {
        double C = RealPart + a.getRealPart();
        double D = ImagePart + a.getImagePart();
        Complex E = new Complex(C, D);
        return E;
    }

    public Complex ComplexSub(Complex a)
    {
        double C = RealPart - a.getRealPart();
        double D = ImagePart - a.getImagePart();
        Complex E = new Complex(C, D);
        return E;
    }

    public Complex ComplexMulti(Complex a)
    {
        double C = RealPart * a.getRealPart() - ImagePart * a.getImagePart();
        double D = RealPart * a.getImagePart() + ImagePart * a.getRealPart();
        Complex E = new Complex(C, D);
        return E;
    }

    public Complex ComplexDiv(Complex a)
    {
        double C = a.getRealPart() * a.getRealPart() + a.getImagePart() * a.getImagePart();
        double D = (RealPart * a.getRealPart() + ImagePart * a.getImagePart()) / C;
        double E = (RealPart * a.getImagePart() * -1 + ImagePart * a.getRealPart()) / C;
        Complex F = new Complex(D, E);
        return F;
    }


    public String toString()
    {
        if (ImagePart < 0) {
            String getter = RealPart + "" + ImagePart + "i";
            return getter;
        } else {
            String setter = RealPart + "+" + ImagePart + "i";
            return setter;
        }
    }


}