package src.shiyan;
/**
 * Demo class
 *
 * @author keriezhang
 * @date 2016/10/31
 */
public class CodeStandard{
   public static void main(String[] args) {
        final int a=20;
        StringBuffer buffer = new StringBuffer();
        buffer.append('S');
        buffer.append("tringBuffer");
        System.out.println(buffer.charAt(1));
        System.out.println(buffer.capacity());
        System.out.println(buffer.indexOf("tring"));
        System.out.println("buffer = " + buffer.toString());
        if (buffer.capacity() < a) {
            buffer.append("1234567");
        }
        for (int i = 0; i < buffer.length(); i++) {
            System.out.println(buffer.charAt(i));
        }
    }
}