package src.shiyan;

abstract class Data
{
    abstract public void DisplayValue();
}
// byte类
class Byte extends  Data
{
    int value;
    //构造方法
    Byte()
    {
        value=20172333%6;
    }
    //  显示value的值
    public void DisplayValue()
    {
        System.out.println (value);
    }
}
//short类
class Short extends  Data
{
    int value;

    Short()
    {
        value = 20172333%6;
    }

    public void DisplayValue()
    {
        System.out.println(value);
    }
}
//Boolean类
class Boolean extends  Data
{
    int value;

    Boolean()
    {
        value = 20172333%6;
    }

    public void DisplayValue()
    {
        System.out.println(value);
    }
}
// long类
class Long extends  Data
{
    int value;

    Long() {
        value = 20172333%6;
    }

    public void DisplayValue()
    {
        System.out.println(value);
    }
}
//float类
class Float extends  Data
{
    int value;

    Float()
    {
        value = 20172333%6;
    }

    public void DisplayValue()
    {
        System.out.println(value);
    }
}
//double类
class Double extends  Data
{
    int value;

    Double()
    {
        value = 20172333%6;
    }

    public void DisplayValue()
    {
        System.out.println(value);
    }
}

//抽象类 pattern classes
abstract class Factory
{
    abstract public Data CreateDataObject();
    //抽象方法
}

class ByteFactory extends Factory
{
    public Data CreateDataObject()
    {
        return new Byte();
    }
}

class ShortFactory extends Factory
{
    public Data CreateDataObject(){
        return new Short();
    }
}

class BooleanFactory extends Factory
{
    public Data CreateDataObject(){
        return new Boolean();
    }
}

class LongFactory extends Factory
{
    public Data CreateDataObject(){
        return new Long();
    }
}

class FloatFactory extends Factory
{
    public Data CreateDataObject(){
        return new Float();
    }
}

class DoubleFactory extends Factory
{
    public Data CreateDataObject(){
        return new Double();
    }
}

//Client  classes
class Document
{
    Data pd;
    Document(Factory pf)
    {
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}

//Test class
 public class MyDoc
{
    static Document q,w,e,r,t,y;
    public static void main(String []args)
    {
        q=new Document(new ByteFactory());
        w=new Document(new ShortFactory());
        e= new Document(new BooleanFactory());
        r = new Document(new LongFactory());
        t = new Document(new FloatFactory());
        y = new Document(new DoubleFactory());
        q.DisplayData();
        w.DisplayData();
        e.DisplayData();
        r.DisplayData();
        t.DisplayData();
        y.DisplayData();


        }
    }
