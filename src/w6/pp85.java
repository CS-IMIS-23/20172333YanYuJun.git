/**
 * Created by 19222 on 2018/4/14.
 * pp8.5.java   Author:YanYuJun
 *
 * 计算X1-Xn 的平均值 和标准方差，假设输入不超过50个值。
 *
 */

import java.util.*;
public class pp85 {
    public static void main(String []args )
    {
        double [] X =  new double[50];
        int a = 0;
        double b;
        int n=0;
        double av;
        double sum=0;
        double sd ;
        double sb =0;
        
        Scanner scan = new Scanner(System.in);
        while (a<50&&a>=0)
        {   
            int c =0;
            System.out.println("依次输入X1到Xn的值最多n=50(即最多五十次输入),若想结束输入X个数则输入666：");
            b = scan.nextDouble();
            if (b==666.0)
                break;
            else
                {
                    
                X[c] = b;
                a++;
                n++;
                c++;
                 }

        }
        for (int k=0;k<n;k++)
            sum += sum + X[k];

        av = sum/n;
        System.out.println("平均数=" +av );

        for (int l=0;l<n;l++) {
            sb = Math.pow(X[l] - av,2);
            sb+=sb;

        }
            sd = Math.sqrt(sb);
        System.out.println("标准方差=" + sd);





    }

}

