/**
 * Created by 19222 on 2018/4/14.
 *L&L银行管理30个账户，随时更新账户，可存取款，错误交易出现错误信息。多定义一个方法来增加百分之三的利息。
 * Author:YanYuJun
 */
import java.util.*;
public class pp86 {

    public static void main(String[]args)
    {
        pp86lei acts = new pp86lei();

        acts.addAcoun("nb",6666,6540);
        acts.addAcoun("lihaile",9999,99999990);

        System.out.println(acts.putMoney(666,20,1));
        //这里应该是取钱的，方法名之前定错了。
        System.out.println(acts.getYue(123,0));
        //这里是存钱定义。
        System.out.println(acts.addInterest());

        System.out.println(acts.toString());


    }

}


