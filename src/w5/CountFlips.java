//----------------------------------------------------------------------------------------------
// CountFlips.java       Author:YanYuJun
//
//throw 100 times coin and get the numbers of heads
//----------------------------------------------------------------------------------------------
  
  public class CountFlips
   {
    public static void main(String[]args)
      {
         final int MAX=100;
         int times =0;
         
        Coin head =new Coin();
         
       for(
           int flip =1;
           flip <=MAX;
           flip ++)
           
       { head.flip();
         if (head.isHeads())
            times++;
       }
       System.out.println(times);
     }
    }
