//-----------------------------------------------------
// pp6.3.java   Author:YanYuJun
//
//-----------------------------------------------------

import java.util.Scanner;

   public  class   pp63
   {
    //------------------------------------------------------------------------
    // prints a 12*12 Mutiplication table to use
    //------------------------------------------------------------------------
    public static void main(String[]args)
     {
      final int MAX =12;
      for (int row =1; row<=MAX; row++)
      {
       for(int star=1; star<=row; star++)
            System.out.print(row*star+" ");
            System.out.println();
        }
     }
    }

