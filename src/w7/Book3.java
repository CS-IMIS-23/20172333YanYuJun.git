package src.w7;
//---------------------------------------------------------------------------------
// pp93.java     Author:YanYuJun
//
//该类用来定义书的页数与关键词与书名，并建立相关方法及其测试。
//---------------------------------------------------------------------------------


import 书上.Dictionary2;


public class Book3 extends Dictionary2{

    String a;

    public Book3(int numPages, int numDefinitions) {
        super(numPages, numDefinitions);
    }
    //用于继承Dictionary2的方法。
    public void  setKey(String b)
    {
        a =b;

    }
    //用于定义输入关键词的方法。
    public String getKey()
    {
        return a;

    }
    //用于定义获取关键词的方法。
    public void setName(String c)
    {
        c =a;
    }
    //用于定义输入书名的方法。
    public String getName()
    {
        return a;
    }
}
