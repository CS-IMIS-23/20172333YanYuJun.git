//------------------------------------------------------------------------------------
// MonetaryCoinTest.java           Author:YanYuJun
//
//用于测试MonetaryCoin是否能够返回硬币面值及求和操作，并演示父类的抛硬币方法。
//-------------------------------------------------------------------------------------


public class MonetaryCoinTest
{
    public static void main(String[]args)
    {
        MonetaryCoin a = new MonetaryCoin(5);

        MonetaryCoin c = new MonetaryCoin(10);

        MonetaryCoin d = new MonetaryCoin(20);



        int q = a.back();
        System.out.println("输出硬币面额：" + q);
        //测试back方法是否有效。


         int w = c.back();
         int e = d.back();
         int r =q + w + e;
         System.out.println("三个MonetaryCoin对象的和：" +r);
        //测试MonetaryCoin的对象的和。

         a.flip();
         a.isHeads();


         System.out.println("抛硬币结果：" + a.toString() );


    }
}
