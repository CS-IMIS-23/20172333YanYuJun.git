package src.w7;

//--------------------------------------------------------------------------
//pp9.3Test.java      Author:YanYuJun
//
//这个Java程序是用来检测Book3的方法是否正确的。
public class NovelTest
{
    public static void main(String []args)
    {
        Book3 a =new Book3(666,50);
        a.setPages(600);
        //设置书本的页数；
        a.setKey("骚操作多");
        //设置书本关键词；
        int q= a.getPages();
        //获取书本的页数；
        String w = a.getKey();

        a.setName("斗罗大陆");

        System.out.println("这本书的书名：" + a.getName());
        System.out.println("这本书的页数：" + q );

        System.out.println("这本书的关键词：" + w);
    }

}