package src.zuoye;

public abstract class Animal {
     String name;
     int id;

    public Animal(String name, int id) {
        this.name = name;
        this.id = id;
    }
    public abstract void eat();
    public abstract void sleep();
    public abstract void introduction() ;
}