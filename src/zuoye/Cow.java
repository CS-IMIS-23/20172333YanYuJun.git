package src.zuoye;

public class Cow extends Animal
{
 public Cow(String name,int id)
 {
     super(name, id);
 }
    @Override
    public void eat()
    {
        System.out.println("Cow正在吃");
    }

    @Override
    public void introduction()
    {
        System.out.println("我是"+ name +"我的号码是" +id);
    }

    @Override
    public void sleep()
    {
        System.out.println("Cow正在睡觉");
    }
}

