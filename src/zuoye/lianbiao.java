package src.zuoye;

//*******************************************************************
//  MagazineList.java       Author: Lewis/Loftus
//
//  Represents a collection of magazines.
//*******************************************************************

public class lianbiao
{
    private MagazineNode list;

    //----------------------------------------------------------------
    //  Sets up an initially empty list of magazines.
    //----------------------------------------------------------------
    public lianbiao()
    {
        list = null;
    }

    //----------------------------------------------------------------
    //  Creates a new MagazineNode object and adds it to the end of
    //  the linked list.
    //----------------------------------------------------------------
    public void add(Magazine mag)
    {
        MagazineNode node = new MagazineNode(mag);
        MagazineNode current;

        if (list == null)
            list = node;
        else
        {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }
    }

    //----------------------------------------------------------------
    //  Returns this list of magazines as a string.
    //----------------------------------------------------------------
    public String toString()
    {
        String result = "";

        MagazineNode current = list;

        while (current != null)
        {
            result += current.magazine + "\n";
            current = current.next;
        }

        return result;
    }

    //*****************************************************************
    //  An inner class that represents a node in the magazine list.
    //  The public variables are accessed by the MagazineList class.
    //*****************************************************************
    private class MagazineNode
    {
        public Magazine magazine;
        public MagazineNode next;

        //--------------------------------------------------------------
        //  Sets up the node
        //--------------------------------------------------------------
        public MagazineNode(Magazine mag)
        {
            magazine = mag;
            next = null;
        }
    }
    public void insert(int index,Magazine newMagazine)
    {
        MagazineNode node =new MagazineNode(newMagazine);
        MagazineNode current = list;
        if (index ==0)
        {
           node.next=current;
           list =node;
        }
        //头部插入。
        else
        {
            for (int j=0;j<index-2;j++)
            {
                current=current.next;

            }
            node.next=current.next;
            current.next=node;
        }
        //中间插入。
    }

    public void delete(int index)
    {
        MagazineNode del =null;
        MagazineNode current = null;
        if (index==0)
        {
            del =list;
            list=list.next.next;
        }
        //删除的头部
        else
        {
            MagazineNode dell=list;
            int o=0;
            while (dell!=null&&o<index-2){
                dell=dell.next;
                o++;
            }
            dell.next=dell.next.next;
        }


    }



}
