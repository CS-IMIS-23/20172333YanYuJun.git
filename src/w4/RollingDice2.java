//****************************************************************8
//PairOfDice test     RollingDice2.java   YanYuJun
//****************************************************************8

 public class RollingDice2
   {
     public static void main(String[] args)
    {
      PairOfDice pd1 = new PairOfDice();
      
      pd1.setP1(2);
      System.out.println("P1:" + pd1.getP1());
      
      pd1.setP2(6);
      System.out.println("P2:" + pd1.getP2());
     
      pd1.roll2();
      System.out.println("P1:" + pd1.getP1() +"\n" + "P2:" + pd1.getP2());
      
      pd1.getSum();
      System.out.println("Sum:" + pd1.getSum());
   }
}
