//************************************************************************************
// CarTest.java        Author:YanYuJun
//************************************************************************************

 public class CarTest
 {
   public static void main(String[] args)
  {
    Car car1;
    car1 = new Car("Tesla Motors","Model 3",4096);
    
    System.out.println("Trade name :" + car1.getTradename());
    System.out.println("Model :" + car1.getType());
    System.out.println("year :" + car1.getYear());
    
    System.out.println("");
    System.out.println("");
    System.out.println("");
    System.out.println("");
    //blank
    System.out.println("Car:" + car1.toString());
    
    System.out.println(" antique:" + car1.isAntique());
  }
}
