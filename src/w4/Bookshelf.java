//-------------------------------------------------------
// Bookshelf.java       Author:YanYuJun
//------------------------------------------------------
 
 public class Bookshelf
{
  private String bookname;
  private String author;
  private String press;
  public  String copyrightdate;
  
  
  public Bookshelf(String nm,String ar,String ps,String ce)
        { 
         bookname = nm;
         author = ar;
         press  = ps;
         copyrightdate = ce;
        }  
  public  String getBookname()
 {
    return bookname;
 }
  public  String getAuthor()
 {
    return author;
 } 
  public  String getPress()
 {
    return press;
 }
  public  String getCopyrightdate()
 {
    return copyrightdate;
 }
  public  void setBookname(String nm)
 {   
    bookname = nm;
 }
  public  void setAuthor(String ar)
 {
    author = ar;
 }
  public  void setPress(String ps)
 {
    press = ps;
 }
  public void setCopyrightdate(String ce)
 {
    copyrightdate = ce;
 }
  
  public String toString()
 {
    return "The bookname is: " + bookname + "\n" +"The author of the book is: " + author + "\n" +
            "The press is:" + press + "\n" + "The copyrightdate of the book is:" +copyrightdate;
 }
}
                            
 
     
