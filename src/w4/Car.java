//*****************************************************************************************************
// Car.java    Author:YanYuJun
//*****************************************************************************************************
  import java.util.Scanner;


   public class Car
 {
   private String tradename;
   private String type;
   private int year;
   public boolean nianfen;
//Set tradename type year
   
   public Car(String name,String kinds,int time)
   {
     tradename = name;
     type   = kinds;
     year   = time;
   }
   
   public String getTradename()
   {
     return tradename;
   }
   
   public String getType()
   {
     return type;
   }
   
   public int getYear()
   {
     return year;
   }
   
   //Use toString method

   public String toString()
   {
     return("Tradename:" + tradename + "\n" +"Type:" + type + "\n" +"Year:" + year );
   }
    
   
  
   public Boolean isAntique()
   {  
	if(year>45){
	   nianfen = true;
	}else{
 	   nianfen = false;   
	}
     return nianfen;
   }
 }
