//**************************************************************************
// PairOfDice.java    Author:YanYuJun
//**************************************************************************
  
 public class PairOfDice
 {
    Die die1 = new Die();
    Die die2 = new Die();
    
    public int p1;
    public int p2; 
    public int sum;  

    public PairOfDice()
    {
     
    }
    
    public int getP1()
    {
     p1 = die1.getFaceValue();
      return p1;
      
    }
    
    public int getP2()
    {  
     p2 = die2.getFaceValue();
     return p2;
    }
    
    public void setP1(int x)
    {
     die1.setFaceValue(x);
     p1 = die1.getFaceValue();
    }
    
    public void setP2(int y)
    {
     die1.setFaceValue(y);
     p2 = die2.getFaceValue();
    }
    
    public void  roll2()
    {
     die1.roll();
     die2.roll();
    
    }
   
    public int getSum()
    {
     sum = die1.getFaceValue() + die2.getFaceValue();
    return sum;

}
  }
