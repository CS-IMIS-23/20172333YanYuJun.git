//-------------------------------------------------------
//pp7.1 Use account name and password to establish account number
// Author:YanYuJun
//----------------------------------------------------------
 
 import java.text.NumberFormat;
    
  public class Account
 {
  
  private long acctNumber;
  private final double RATE = 0.035;
  private double balance;
  private String name;
   
  //-------------------------------------------\
  //Sets up the account by defining its owner, account number,
  // and initial balance.
  //------------------------------------------------------------
 
  public Account(String owner, long account )
   { 
     name= owner;
     acctNumber = account;
    
   }
   
   //-------------------------------------------------------------
   // Deposits the specified amount into the account, Returns the 
   // new balance.
   //------------------------------------------------------------
   
   public double deposit(double amount)
   {
    balance = balance = amount;
    return balance;
   }

   //-------------------------------------------------------------------
   // Withdraws the specified amount from the account and app;oes
   // the fee. Returns the new balance.
   // -------------------------------------------------------------------

   public  double withdraw(double amount,double fee)
   {
     balance = balance - amount - fee;
     
     return balance;
   }

   //----------------------------------------------------------------------
   //  Adds interest to the account and returns the new balance.
   //----------------------------------------------------------------------

   public double addInterest()
   {
     balance +=(balance*RATE);
     return balance;
   }
   
   //----------------------------------------------------------------------
   // Return the current balance of the acoount,
   //----------------------------------------------------------------------
   public double getBalance()
   { 
     return balance;
   }

   //----------------------------------------------------------------------
   // Returns a one-line description of the account as a string.
   // ---------------------------------------------------------------------
   public String toString ()
   {
  
     NumberFormat fmt =NumberFormat.getCurrencyInstance();
     return acctNumber+ "\t" +name +"\t" + fmt.format(balance);
   }
}






