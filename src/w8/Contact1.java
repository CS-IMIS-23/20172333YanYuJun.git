package src.w8;

//********************************************************************
//  Contact.java       Author: Lewis/Loftus
//
//  Represents a phone contact.
//********************************************************************

public class Contact1 implements Comparable
{
    private String title,director;
    int year;
    double cost;
    boolean bluray;

    //-----------------------------------------------------------------
    //  Constructor: Sets up this contact with the specified data.
    //-----------------------------------------------------------------
    public Contact1(String title1, String director1, int year1,double cost1,boolean bluray1)
    {
        title = title1;
        director = director1;
        year = year1;
        cost = cost1;
        bluray=bluray1;
    }

    //-----------------------------------------------------------------
    //  Returns a description of this contact as a string.
    //-----------------------------------------------------------------
    public String toString()
    {
        return title + " " + director + "\t" + year;
    }

    //-----------------------------------------------------------------
    //  Returns a description of this contact as a string.
    //-----------------------------------------------------------------
    public boolean equals(Object other)
    {
        return (director.equals(((Contact1)other).getLastName()) &&
                title.equals(((Contact1)other).getFirstName()));
    }

    //-----------------------------------------------------------------
    //  Uses both last and first names to determine ordering.
    //-----------------------------------------------------------------
    public int compareTo(Object other)
    {
        int result;

        String otherFirst = ((Contact1)other).getFirstName();
        String otherLast = ((Contact1)other).getLastName();

        if (director.equals(otherLast))
            result = title.compareTo(otherFirst);
        else
            result = director.compareTo(otherLast);

        return result;
    }

    //-----------------------------------------------------------------
    //  First name accessor.
    //-----------------------------------------------------------------
    public String getFirstName()
    {
        return title;
    }

    //-----------------------------------------------------------------
    //  Last name accessor.
    //-----------------------------------------------------------------
    public String getLastName()
    {
        return director;
    }
}
