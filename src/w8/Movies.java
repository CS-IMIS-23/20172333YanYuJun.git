package src.w8;
//-------------------------------------------------------------------
//PP10.5 Author:YanYuJun
//
//改编第八章Movies程序，将所存储的DVD按名字排序。
//-------------------------------------------------------------------

public class Movies
{

    //-----------------------------------------------------------------------------------------------------------
    //  Creates a DVDCollection object and adds some DVDs to it. Prints reports on the status of the collection.
    //-----------------------------------------------------------------------------------------------------------
    public static void main(String[] args)
    {

        Contact1[] Moives = new Contact1[7];



        Moives[0]=new Contact1("The Godfather", "Francis Ford Coppola", 1972, 24.95, true);
        Moives[1]=new Contact1("District 9", "Neill Blomkamp", 2009, 19.95, false);
        Moives[2]=new Contact1("Iron Man","Jon Favreau", 2008, 15.95, false);
        Moives[3]=new Contact1("All About Eve", "Joseph Mankiewicz", 1950, 17.50, false);
        Moives[4]=new Contact1("The Matrix", "Andy & Lana Wachowski", 1999, 19.95, true);
        Moives[5]=new Contact1("Iron Man 2","Jon Favreau", 2010, 22.99, false);
        Moives[6]=new Contact1("Casablanca","Michael Curtiz", 1942, 19.95, false);

        System.out.println("用名字倒序排序：");
        Sorting1.selectionSort(Moives);//单词拼错了但是不想改了。

        for (Contact1 Moive :Moives)
            System.out.println(Moive);

        System.out.println("以名字顺序排列：");

        Sorting.selectionSort(Moives);

        for (Contact1 Moive1 :Moives)
            System.out.println(Moive1);


    }
}