package src.w8;
//pp9.4修改Sorting1后的Main测试类。

public class Sorting1Test
{
    public static void main(String[]args)
    {
        src.w8.Contact[] friends = new src.w8.Contact[8];

        friends[0] = new src.w8.Contact("John", "Smith", "610-555-7384");
        friends[1] = new src.w8.Contact("Sarah", "Barnes", "215-555-3827");
        friends[2] = new src.w8.Contact("Mark", "Riley", "733-555-2969");
        friends[3] = new src.w8.Contact("Laura", "Getz", "663-555-3984");
        friends[4] = new src.w8.Contact("Larry", "Smith", "464-555-3489");
        friends[5] = new src.w8.Contact("Frank", "Phelps", "322-555-2284");
        friends[6] = new src.w8.Contact("Mario", "Guzman", "804-555-9066");
        friends[7] = new src.w8.Contact("Marsha", "Grant", "243-555-2837");

        System.out.println("选择排序法：");

        Sorting1.selectionSort(friends);
        for (src.w8.Contact friend : friends)
            System.out.println(friend);
        System.out.println();
        System.out.println("插入排序法");
        Sorting1.insertionSort(friends);
        for(src.w8.Contact friend:friends)
            System.out.println(friend);
    }
}
