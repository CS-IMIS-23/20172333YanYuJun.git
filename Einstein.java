//*******************************************************************
// Einstein.java              Author.Lewis/Loftus
//
// Demonstrates a basic applet.
//*******************************************************************

import javax.swing.JApplet;
import java.awt.*;
 
 public class Einstein extends JApplet
 {
   //---------------------------------------------------------
   // Draws a quotation by Albert Eingsstein among some shapes.
   //---------------------------------------------------------
 public static void paint(Graphics page)
{ 
 page.drawRect(50,50,40,40);
 page.drawRect(60,80,225,30);
 page.drawOval(75,65,20,20);
 page.drawLine(35,60,100,120);
 
 page.drawString("Out of clutter,fing simplicity.",110,70);
 page.drawString("-- Ablert Einstein", 130,100);
 }
}
