//------------------------------------------------
//  Num.java  Author:Lewis/loftus
//------------------------------------------------

  public class Num
 {
   private int value;
   
    //---------------------------------------------------
    // Sets up tje mew Num object,storing an initial value.
    // ----------------------------------------------------
   public Num(int update)
    {
      value = update;
    }
    
    //----------------------------------------------------
    //  Sets the stored value to the newly specified value.
    //----------------------------------------------------
    public void setValue(int update)
    {

      value = update;
    }
    
    //-----------------------------------------------------
    //  return the stored integer value as a string.
    //-----------------------------------------------------
    public String toString()
    {
      return value + "";  
    }
 }
