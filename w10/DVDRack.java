package w10;

public class DVDRack
{
    public static void main(String[]args)
    {
        DVDList rack = new DVDList();

        rack.add(new DVD("S","Youtube",2015,60,true));
        rack.add(new DVD("C","Ed Sheeran",2017,60,true));

        System.out.println(rack);
    }
}
