package w10;
//-------------------------------------
//
//PP13.1 Author：YanYuJun
//
//-------------------------------------
public class DVDList
{
    public DVDNode list;

    public DVDList()
    {
        list = null;
    }

    public void add(DVD d)
    {
        DVDNode node = new DVDNode(d);
        DVDNode current;

        if(list == null)
        {
            list = node;
        } else
        {
            current = list;
            while(current.next!=null)
            {
                current = current.next;
            }
            current.next = node;
        }
    }
    @Override
    public String toString()
    {
        String result = "";
        DVDNode current = list;
        while(current != null)
        {
            result += current.dvd + "\n";
            current = current.next;
        }
        return result;
    }

    private class DVDNode {
        public DVD dvd;
        public DVDNode next;

        public DVDNode(DVD dv) {
            dvd = dv;
            next = null;
        }
    }
}