//****************************************
//  PianoKeys.java       Author:Lewis/Loftus
//
//  Demonstrates the declaration, initialization, and use of an 
//  integer variable
//*****************************************************************
 
public class PianoKeys 
{
 public static void main(String[] args)
 {
  int keys =66;
    System.out.println("A piano has " + keys + "keys.");
 }
}
