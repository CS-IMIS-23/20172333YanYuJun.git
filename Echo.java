//*************************************************************************
// Echo.java           Author:Lewis/Loftus
//
// Demonstratrs the use of the nextLine method of the Scanner class
// to read a string from the user.
//*************************************************************************

import java.util.Scanner;

 public class Echo
{
 //-------------------------------------------------------------------------------
 // Reads a character string from the user and prints it.
 //-------------------------------------------------------------------------------
 public static void main(String[] args)
 {
 String message;
 Scanner scan = new Scanner(System.in);
 message = scan.nextLine();
 System.out.println("You entered: \""  + message + "\"");
}
}
