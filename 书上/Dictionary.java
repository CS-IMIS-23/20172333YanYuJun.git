package 书上;
//-----------------------------------------------------------------------------------
// Dictionary.java  Author:YanYuJun
//
// Represents a dictionary, which is a book .used to demonstrate
//inheritance.
//------------------------------------------------------------------------------------

public class Dictionary extends Book
{
    private int definitions =52500;

    //-------------------------------------------------------------------
    // Prints a message using both local and inherited va;ies.
    //-------------------------------------------------------------------
    public double computeRatio()
    {
        return (double) definitions/pages;
    }

    //-------------------------------------------------------------------
    // Definitions mutator.
    //-------------------------------------------------------------------
    public  void  setDefinitions(int numDefinitions)
    {
        definitions = numDefinitions;
    }

    //-------------------------------------------------------------------
    //Definitions accessor
    //-------------------------------------------------------------------
    public int getDefinitions()
    {
        return definitions;
    }

}
