package 书上;
//------------------------------------------------------------------------
// Dictionary2.java Author:YanYuJun
//
//Represents a dictionary,which is a book, Used to demonstrate
//the use of the super reference.
//------------------------------------------------------------------------
public class Dictionary2 extends Book2{
    private int definitions;

    //---------------------------------------------------------------------
    // Prints a message using both local and inherited values.
    //---------------------------------------------------------------------
    public Dictionary2(int numPages,int numDefinitions)
    {
        super(numPages);

        definitions = numDefinitions;

    }
    //---------------------------------------------------------------------
    // Prints a message using both local and inherited values.
    //---------------------------------------------------------------------
    public double computeRatio()
    {
        return (double) definitions/pages;
    }

    //---------------------------------------------------------------------
    // Definitions accessor.
    //---------------------------------------------------------------------
    public int getDefinitions()
    {
        return definitions;
    }

}


